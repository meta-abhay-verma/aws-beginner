'use strict';

const zlib = require('zlib');
const AWS = require("aws-sdk");

const s3 = new AWS.S3({
  accessKeyId: process.env.DEV_AWS_ACCESS_KEY,
  secretAccessKey: process.env.DEV_AWS_SECRET_ACCESS_KEY,
});

const uploadContent = (fileName, body) => {
  const params = {
    Bucket:
      "<bucket_name>", // pass your bucket name
    Key: fileName,
    Body: body,
  };
  s3.upload(params, function (s3Err, data) {
    if (s3Err) throw s3Err;
    console.log(`File uploaded successfully at ${data.Location}`);
  });
};


// entry point
exports.handler = (event, context, callback) => {
    console.log('working');
    const payload = Buffer.from(event.awslogs.data, 'base64');

    // converts the event to a valid JSON object with the sufficient infomation required
    function parseEvent(logEvent, logGroupName, logStreamName) {
        return {
            // remove '\n' character at the end of the event
            message: logEvent.message.trim(),
            logGroupName,
            logStreamName,
            timestamp: new Date(logEvent.timestamp).toISOString(),
        };
    }

    // joins all the events to a single event
    // and sends to Loggly using bulk endpoint
    function postEventsToS3(parsedEvents) {

        // get all the events, stringify them and join them
        // with the new line character which can be sent to Loggly
        // via bulk endpoint
        const finalEvent = parsedEvents.map(JSON.stringify).join('\n');
        console.log(finalEvent);
        uploadContent('export'+Date.now().toString(), finalEvent)
    }

    zlib.gunzip(payload, (error, result) => {
        if (error) {
            callback(error);
        } else {
            const resultParsed = JSON.parse(result.toString('ascii'));
            const parsedEvents = resultParsed.logEvents.map((logEvent) =>
                    parseEvent(logEvent, resultParsed.logGroup, resultParsed.logStream));

            postEventsToS3(parsedEvents);
        }
    });
};
