# AWS - Beginner

[Link to the Presentation](https://docs.google.com/presentation/d/1Mcp_CN04cuUl7TN6R03HCDqsdnCIWFo7w_4gSkh5uFQ/edit#slide=id.g35f391192_00)
- Create a new AWS Lambda function
- Add an API Gateway trigger

![assets/Untitled.png](assets/Untitled.png)

- Create a free tier mysql db
- init db

```sql
mysql -u admin -p'Letmein$12345#' -h metacube.ciduxmgpb84b.us-east-2.rds.amazonaws.com -P 3306
mysql> create database metacube;
mysql> use metacube;
mysql> CREATE TABLE user (id INTEGER AUTO_INCREMENT PRIMARY KEY, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL);

```

- Create your logic in any language of choice and upload it to AWS.
- Use this command to hit the get request:

```bash
curl 'https://2j1a9j0l56.execute-api.us-east-2.amazonaws.com/default/aws_beginner_APIs_202010051859'
```

- Use this command to hit the post request, change the data as it suits you:

```bash
curl -d '[{"firstName":"Abhay","lastName":"_"},{"firstName":"Abhay","lastName":"Verma"}]' -H 'Content-Type: application/json' 'https://2j1a9j0l56.execute-api.us-east-2.amazonaws.com/default/aws_beginner_APIs_202010051859'
```

- Use this command to hit the get request and get user by id:

```bash
curl 'https://2j1a9j0l56.execute-api.us-east-2.amazonaws.com/default/aws_beginner_APIs_202010051859?id=1'
```

- Create a new function with following cloudwatch trigger:

![assets/Untitled%201.png](assets/Untitled%201.png)

- Here are the main logs saved automatically in cloudwatch:

![assets/Untitled%202.png](assets/Untitled%202.png)

- Here are the redirected logs in S3:

![assets/Untitled%203.png](assets/Untitled%203.png)

- IP range to CIDR

![assets/Untitled%204.png](assets/Untitled%204.png)

- Created a policy accordingly in resource policy section of the API gateway:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": "*",
            "Action": "execute-api:Invoke",
            "Resource": "arn:aws:execute-api:us-east-2:194345035304:2j1a9j0l56/*/*/aws_beginner_APIs_202010051859",
            "Condition": {
                "IpAddress": {
                    "aws:SourceIp": [
                        "203.129.200.114/31",
                        "203.129.200.116/30",
                        "203.129.200.120/30",
                        "203.129.200.124/31",
                        "203.129.200.126/32"
                    ]
                }
            }
        }
    ]
}
```

- After creating the policy, response from my system:

![assets/Untitled%205.png](assets/Untitled%205.png)

- Response from WRS server:

![assets/Untitled%206.png](assets/Untitled%206.png)