import mysql.connector
from mysql.connector import Error
import json
import os
host = os.environ['DB_HOST']
db = os.environ['DB_DATABASE']
user = os.environ['DB_USER']
password = os.environ['DB_PASSWORD']
port = os.environ['DB_PORT']

try:
    connection = mysql.connector.connect(host=host,
                                         database=db,
                                         user=user,
                                         password=password, port=port)
    user_insert_query = "INSERT INTO user (firstName, lastName) VALUES (%s, %s);"
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        result = cursor.execute(user_insert_query, ("Abhay", "Very Good IS THE"))
        connection.commit()
        print("You're connected to database: ", result)

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")