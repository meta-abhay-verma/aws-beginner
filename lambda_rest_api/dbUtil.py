import mysql.connector
from mysql.connector import Error
import os

host = os.environ['DB_HOST']
db = os.environ['DB_DATABASE']
user = os.environ['DB_USER']
password = os.environ['DB_PASSWORD']
port = os.environ['DB_PORT']

def has(obj, key):
    return (key in obj) and (obj[key] is not None)

def hasId(ev):
    return has(ev, 'queryStringParameters') and has(ev['queryStringParameters'], 'id') and int(ev['queryStringParameters']['id']) > 0

def rowToObject(row):
    print(row)
    return {'id': row[0], 'firstName': row[1], 'lastName': row[2]}

def rowsToObjects(rows):
    return list(map(rowToObject, rows))

def getConnection():
    connection = mysql.connector.connect(host=host,
                                         database=db,
                                         user=user,
                                         password=password, port=port)
    return connection

def getUserById(id):
    cursor = None
    try:
        connection = getConnection()
        user_fetch_query = "SELECT * FROM user" + (" WHERE id = " + str(id) if id is not None else "") + ";"
        print(user_fetch_query)
        cursor = connection.cursor()
        cursor.execute(user_fetch_query)
        result = rowToObject(cursor.fetchone()) if id is not None else rowsToObjects(cursor.fetchall())
    except Error as e:
        print("Error while fetching from MySQL: ", e)
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
    return result

def saveUsers(users):
    cursor = None
    try:
        connection = getConnection()
        cursor = connection.cursor()
        user_insert_query = "INSERT INTO user (first_name, last_name) VALUES " + ",".join(list(map(lambda user: "('" + user.get('firstName') + "','" + user.get('lastName') + "')", users))) + ";"
        print(user_insert_query)
        cursor.execute(user_insert_query)
        connection.commit()
    except Error as e:
        print("Error while connecting to MySQL", e)
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
    return True
    