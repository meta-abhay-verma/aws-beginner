create database metacube;
use metacube;

CREATE TABLE user (id INTEGER AUTO_INCREMENT PRIMARY KEY, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL);