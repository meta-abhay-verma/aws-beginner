import json
from mysql.connector import Error
from dbUtil import *

def lambda_handler(event, context):
    print(event)
    if (event.get('httpMethod') == "POST"):
        body = json.loads(event.get('body'))
        result = saveUsers(body)
    else:
        result = getUserById(event['queryStringParameters']['id'] if hasId(event) else None)
    return {
        'statusCode': 200,
        'body': json.dumps(result)
    }
